const env = require("./env.environment.json");
const puerto = env.development.PORT;

const express = require("express");
const server = express();

server.use(express.urlencoded({extended:true}));
server.use(express.json());

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
const swaggerOptions = {
   swaggerDefinition: {
       info: {
           title: 'Sprint Proyect 1',
           version: '1.0.0'
       }
   },
   apis: ['./server.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);
server.use('/swagger',swaggerUI.serve,swaggerUI.setup(swaggerDocs));

var moment = require("moment");


var pedidos = [
    {
       num:1,
       estado:"CONFIRMADO",
       hora:"7:10 pm",
       descripcion:"X1:Hamburguesa Clasica",
       forma_de_pago:"DEBITO",
       total:12000,
       user:{
           id:1,
           user:"admin",
           nom_apell:"nombre y apellido",
           email:"admin@gmail.com",
           tel:0,
           direc:"-",
           passw:"admin",
           admin:true,
           status:false

       }
    }
  
];

var usuarios = [
        {
        id:1,
        user:"admin",
        nom_apell:"nombre y apellido admin",
        email:"admin@gmail.com",
        tel:0,
        direc:"-",
        passw:"admin",
        admin:true,
        status:false
        }
];


var productos = [
    {
        id:1,
        nombre:"Casuela de mariscos",
        precio:"14000"
    },
    {
        id:2,
        nombre:"Hamburguesa Clasica",
        precio:"12000"
    },
    {
        id:3,
        nombre:"Langostinos",
        precio:"25000"
    },
    {
        id:4,
        nombre:"Mojarra frita",
        precio:"10000"
    },
    {
        id:5,
        nombre:"churrasco",
        precio:"17000"
    },
    {
        id:6,
        nombre:"Salchipapas",
        precio:"8000"
    }
];


var FormasdePago = [
    {
        id:1,
        nom:"EFECTIVO"
    },
    {
        id:2,
        nom:"DEBITO"
    },
    {
        id:3,
        nom:"CREDITO"
    },
];

var estados = [
    {
        id:1,
        nom:"PENDIENTE"
    },
    {
        id:2,
        nom:"CONFIRMADO"
    },
    {
        id:3,
        nom:"EN PREPARACION"
    },
    {
        id:4,
        nom:"ENVIADO"
    },
    {
        id:5,
        nom:"ENTREGADO"
    }
];

//Midleware

function validarDatosUsuario_Login(req,res,next){
    const{user,nom_apell,email,tel,direc,passw}=req.body;
    switch(req.url){
        case "/usuarios/alta":
            if(!user || !nom_apell || !email || !tel || !direc || !passw){
                res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS");
            }else{
                if(user!=="" || nom_apell!=="" ||email!=="" || tel!=="" || direc!=="" || passw!=="" ){

                    let ban = usuarios.find(item => item.email === email.toLowerCase())
                    if(ban!==undefined){
                        res.status(404).json("EL EMAIL YA LO POSEE OTRO USUARIO");
                    }else{
                        next()
                    }
                }else{
                    res.status(404).json("LOS CAMPOS NO DEBEN ESTAR VACIOS ")
         
                }
            }
            break;
          case "/usuarios/login":
              if(!user || !passw){
            res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS");

       
        }else{
            if(user !=="" || passw !==""){

                usuarios.map(item => {
                    if(item.status===true){
                        item.status = false;
                        
                    }
                 });

                 usuarios.map(item => {
                     if(item.user===user&&item.passw===passw){
                         item.status = true;
                         next()

                     }
                 });

               res.status(404).json("NO SE ENCONTRO AL USUARIO")
               }else{
                   res.status(404).json("LOS CAMPOS NO DEBEN ESTAR VACIO")
               }

            }
            break;   
      
        
        }   
};

function is_admin(req,res,next){
    let ban = usuarios.find(item => (item.status == true)&&(item.admin == true))
    if(ban===undefined){
        res.status(404).json("DEBE SER ADMIN PARA ACCEDER")
    }else{
        next()
    }
}

function validarDatosPedidos(req,res,next){
    const {pedidos_,forma_de_pago}=req.body;
    if(!pedidos_ || !forma_de_pago){
        res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS")
    }else{
        if(pedidos_!=="" || forma_de_pago!==""){
            next()
        }else{
            res.status(404).json("LOS CAMPOS NO DEBEN ESTAR VACIOS")
        }
    }
}

function validarExistenciaEstado(req,res,next){
    let ban = [];
    ban = estados.filter(item => item.nom === req.body.estado)
    if(ban.length!==0){
        next()
    }else{
        res.status(404).json(`LOS ESTADOS DEBEN SER LOS SIGUIENTES:"PENDIENTE","CONFIRMADO","EN PREPARACION","ENVIADO","ENTREGADO"`)
    }
}

function validarDatosPedidosEstado(req,res,next){
    const{estado}=req.body;
    if(!parseInt(req.params.id) || !estado){
        res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS")
    }else{
        if(parseInt(req.params.id)!=="" || estado!==""){
            next()

        }else{
            res.status(404).json("LOS DATOS NO DEBEN ESTAR VACIOS")
        }
        
    }
}

function validarDatosProducto(req,res,next){
    const{nombre,precio}=req.body;
    if(!nombre || !precio){
        res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS")
    }else{
        if(nombre!=="" || precio!==""){
            next()
        }else{
            res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS RELLENOS")
        }
    }
}

function validarExistenciaProducto(req,res,next){
    let found;
    switch(req.url){
        case "/pedidos/alta":
          const{pedidos_}=req.body;
          pedidos_.forEach((item)=>{
              found = productos.find(x => (x.id === item.producto.id)&&(x.nombre === item.producto.nombre)&&(x.precio === item.producto.precio))
              if(found===undefined){
                  res.status(404).json(`EL PRODUCTO DE ID:${item.producto.id},NOMBRE:${item.producto.nombre},Y PRECIO:${item.producto.precio} NO EXISTE`)
              }
          });
          next(); 
          break;
          default:
              const {nombre,precio}=req.body;
              found = productos.find(x => (x.nombre === nombre)&&(x.precio === precio))
              if(found!==undefined){
                  res.status(404).json(`EL PRODUCTO DE NOMBRE:${nombre}, Y PRECIO:${precio} YA EXISTE`)
              }else{
                  next();
              }
          break;



    }

}

function validarExistenciaFormadePago(req,res,next){
    let ban = [];
    const{formadepago}=req.body;
    switch(req.url){
        case "/pedidos/alta":
            ban = FormasdePago.filter(item => item.nom === req.body.forma_de_pago)
            if(ban.length!==0){
                next();
            }else{
                res.status(404).json(`FORMA DE PAGO NO ESTA DISPONILBE`)
            }

            break;
            case "/medios_de_pago/alta":
                ban = FormasdePago.find(item => item.nom === formadepago.toLowerCase())
                if(ban!==undefined){
                    res.status(404).json(`El medio de pago ya existe`)
                }else{
                    next()
                }
                break;
                default:
                   ban = FormasdePago.find(item => item.id === parseInt(req.params.id))
                   if(ban===undefined){
                       res.status(404).json(`LAS FORMA DE PAGO DE ID(${parseInt(req.params.id)})NO EXISTE`)
                   }else{
                       next()
                   } 
                   break;
                
    }
}

function validarDatosMediosPago(req,res,next){
    const {formadepago}=req.body;
    switch(req.url){
        case"/medios_de_pago/alta":
        if(!formadepago){
            res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS")
        }else{
            if(formadepago !==""){
                next()
            }else{
                res.status(404).json("LOS CAMPOS NO DEBEN ESTAR VACIOS")
            }
        }
        break;
        case`/medios_de_pago/modificar/${req.params.id}`:
         if(!formadepago || !parseInt(req.params.id)){
             res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS")
         }else{
             if(formadepago !==""|| parseInt(req.params.id)!==""){
                 next()
             }else{
                 res.status(404).json("LOS CAMPOS NO DEBEN ESTAR VACIOS")
             }
         }
         break;
         case`/medios_de_pago/eliminar/${req.params.id}`:
         if(!parseInt(req.params.id)){
             res.status(404).json("SE REQUIEREN TODOS LOS CAMPOS")
         }else{
             if(parseInt(req.params.id)!==""){
                 next()
             }else{
                 res.status(404).json("LOS CAMPOS NO DEBEN ESTAR VACIOS")
             }
         }
         break;
    }
}
/**
 * @swagger
 *  /usuarios/alta:
 *   post:
 *    descripcion: Creacion de usuario
 *    parameters:
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description: NOMBRE DE USUARIOS
 *    - name: nom_apell
 *      type: string
 *      in: formData
 *      required: true
 *      description: NOMBRE Y APELLIDO
 *    - name: email
 *      type: string
 *      in: formData
 *      required: true
 *      description: CORREO ELECTRONICO
 *    - name: tel
 *      type: integer
 *      in: formData
 *      required: true
 *      description: TELEFONO  
 *    - name: direc
 *      type: string
 *      in: formData
 *      required: true
 *      description: DIRECCION DE ENVIO
 *    - name: passw
 *      type: string
 *      in: formData
 *      required: true
 *      description: CONTRASEÑA
 *    responses:
 *      200:
 *        description: Sucess 
 * 
 */   
server.post("/usuarios/alta",validarDatosUsuario_Login,(req,res)=>{
    const datos = {
        id:(usuarios.length)+1,
        user:req.body.user,
        nom_apell:req.body.nom_apell,
        email:req.body.email,
        tel:req.body.tel,
        direc:req.body.direc,
        passw:req.body.passw,
        admin:false,
        status:false
    }
    usuarios.push(datos);
    res.status(201).json("SE CREO EXITOSAMENTE EL NUEVO USUARIO")
});

/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    descripcion: INICIO DE SESION
 *    parameters:
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description: USUARIO
 *    - name: passw
 *      type: string
 *      in: formData
 *      required: true
 *      description: CONTRASEÑA
 *    responses:
 *      200:
 *        Sucess 
 */
server.post("/usuarios/login",validarDatosUsuario_Login,(req,res)=>{
    res.status(202).json("SE HA LOGEADO EXITOSAMENTE");
});

//MIDDLEWARE DE LOGEO
server.use(function esta_logeado(req,res,next){
    let ban = usuarios.find(item => item.status == true)
    if(ban===undefined){
        res.status(404).json("DEBE ESTAR LOGEADO PARA ACCEDER")
    }else{
        next()
    }
});

/**
 * @swagger
 * /usuarios/listado:
 *  get:
 *   descripcion: Listado de Usuarios
 *   responses:
 *     200:
 *       Sucess
 */
server.get("/usuarios/listado",is_admin,(req,res)=>{
    res.status(202).json(usuarios);

});


//PRODUCTOS
/**
 * @swagger
 * /productos/alta:
 *  post:
 *    description: AGREGAR PRODUCTO
 *    parameters:
 *    - name: nombre
 *      type: string
 *      in: formData
 *      required: true
 *      description: NOMBRE
 *    - name: precio
 *      type: string
 *      in: formData
 *      required: true
 *      description: PRECIO
 *    responses:
 *      200:
 *        Sucess      
 */
server.post("/productos/alta",is_admin,validarDatosProducto,validarExistenciaProducto,(req,res)=>{
    const datos = {
         id:productos[productos.length-1].id + 1,
         nombre:req.body.nombre,
         precio:req.body.precio

        
    }
    productos.push(datos)
    res.status(201).json("SE REGISTRO EL PRODUCTO EXITOSAMENTE")

});

/**
 * @swagger
 * /productos/modificacion/{id}:
 *  put:
 *    description: Modificacion del producto
 *    parameters:
 *    - name: id
 *      type: number
 *      in: path
 *      required: true
 *      description: ID
 *    - name: nombre
 *      type: string
 *      in: formData
 *      required: true
 *      description: NOMBRE
 *    - name: precio
 *      type: number
 *      in: formData 
 *      required: true
 *      description: PRECIO
 *    responses:
 *      200:
 *        Sucess  
 */
server.put("/productos/modificacion/:id",is_admin,validarDatosProducto,validarExistenciaProducto,(req,res)=>{
    productos.map((item) =>{
        if(item.id === parseInt(req.params.id)){
            item.id=parseInt(req.params.id),
            item.nombre=req.body.nombre,
            item.precio=req.body.precio
       }
        
    
})
    res.status(201).json("SE ACTUALIZO EL PRODUCTO EXITOSAMENTE") 

});

/**
 * @swagger
 * /productos/eliminacion/{id}:
 *  delete:
 *   description: Eliminacion del producto
 *   parameters:
 *   - in: path
 *     name: id
 *     type: integer
 *     required: true
 *     description: ID
 *   responses:
 *     200:
 *       Sucess
 */
server.delete("/productos/eliminacion/:id", is_admin,validarExistenciaProducto,(req,res)=>{
  productos = productos.filter(item => item.id !== parseInt(req.params.id))
  res.status(201).json("SE ELIMINO EL PRODUCTO EXISTOSAMENTE")  
})

/**
 * @swagger
 * /productos/listado:
 *  get:
 *    descripcion: Listado de productos
 *    responses:
 *      200:
 *        Sucess
 */
server.get("/productos/listado",(req,res)=>{
    res.status(202).json(productos);
});

//PEDIDOS

/**
 * @swagger
 * /pedidos/alta:
 *  post:
 *   description: REGISTRAR PEDIDO
 *   parameters:
 *   - name: forma_de_pago
 *     type: string
 *     in: formData
 *     required: true
 *     enum: ["EFECTIVO", "CREDITO", "DEBITO"]
 *     description: FORMA DE PAGO
 *   - name: producto
 *     type: string
 *     in: formData
 *     required: true
 *     enum: ["Casuela de mariscos","Hamburguesa Clasica","Langostinos","Mojarra frita","churrasco","Salchipapas"]
 *     description: PRODUCTOS
 *   - name: cantidad
 *     type: integer
 *     in: formData
 *     required: true
 *     description: CANTIDAD
 *   responses:
 *     200:
 *       Sucess 
 * 
 */  
server.post("/pedidos/alta",validarDatosPedidos,validarExistenciaFormadePago,validarExistenciaProducto,(req,res)=>{
    const{pedidos_,forma_de_pago}=req.body;
    let hour = moment().format("LT");
    let usuario = usuarios.find(item => item.status === true)
    let desc = String();
    let total = new Float32Array();
    pedidos_.forEach(item =>{
        desc = desc + `X${item.cantidad}:${item.producto.nombre};`
        total = total + parseFloat(item.cantidad * item.producto.precio)
    })
    
    pedido = {
        num:(pedidos.length) + 1,
        estado:"CONFIRMADO",
        hora:hour,
        descripcion:desc,
        forma_de_pago:forma_de_pago,
        total:total,    
        user:usuario
    }

    //console log(pedido)
    pedidos.push(pedido);
    res.status(202).json("PEDIDO REGISTRADO")

});

/**
 * @swagger
 * /pedidos/historial:
 *  get:
 *    description: Historial de pedidos de un usuario
 *    responses:
 *      200:
 *        Sucess
 */
server.get("/pedidos/historial",(req,res)=>{
    let usuario;
    usuarios.forEach(item=>{
    if(item.status===true){
        usuario = {
            id:item.id,
            admin:item.admin,
            user:item.user
        }
    }
})
if(usuario.admin == true){
    pedidos.length!==0?res.status(202).json(pedidos):res.status(404).json(`NO SE REGISTRARON PEDIDOS DE NINGUN USUARIO`);
}else{
    let pedidos_user = pedidos.filter(x => x.user.id === parseInt(usuario.id))
    pedidos_user.length!==0?res.status(202).json(pedidos_user):res.status(404).json(`NO SE REGISTRARON PEDIDOS DEL USUARIO:${usuario.user}`);
}

});
/**
 * @swagger
 * /pedidos/modificar/estado/{id}:
 *  put:
 *    description: Actualizacion del estado
 *    parameters:
 *    - name: id
 *      type: string
 *      in: path
 *      required: true
 *      description: ID
 *    - name: estado
 *      type: string
 *      in: formData
 *      required: true
 *      description: ESTADO
 *    responses:
 *      200:
 *        Sucess
 */
server.put("/pedidos/modificar/estado/:id",is_admin,validarDatosPedidosEstado,validarExistenciaEstado,(req,res)=>{
    pedidos.map((item)=>{
        if(item.num === parseInt(req.params.id)){
            item.num = parseInt(req.params.id)
            item.estado = req.body.estado;
        }
    })
    res.status(202).json("ESTADO DE PEDIDO MODIFICADO EXITOSAMENTE")
});

//MEDIOS DE PAGO
/**
 * @swagger
 * /medios_de_pago/alta:
 *  post:
 *    description: AGREGAR MEDIO DE PAGO
 *    parameters:
 *    - name: formadepago
 *      type: string
 *      in: formData
 *      required: true
 *      descripcion: NOMBRE
 *    responses: 
 *      200:
 *        Sucess
 */
server.post("/medios_de_pago/alta",is_admin,validarDatosMediosPago,validarExistenciaFormadePago,(req,res)=>{
    const{formadepago}=req.body;
    const dat = {
        id:FormasdePago[FormasdePago.length-1].id + 1,
        nom:formadepago
    }
    FormasdePago.push(dat);
    res.status(202).json("MEDIO DE PAGO REGISTRADO")
})

/**
 * @swagger
 * /medios_de_pago/listado:
 *  get:
 *    description: LISTADO MEDIOS DE PAGO
 *    responses:
 *      200:
 *        Sucess
 */
server.get("/medios_de_pago/listado",is_admin,(req,res)=>{
    res.status(202).json(FormasdePago)
})

/**
 * @swagger
 * /medios_de_pago/eliminar/{id}:
 *  delete:
 *   description: ELIMINAR MEDIO DE PAGO
 *   parameters:
 *   - name: id
 *     type: string
 *     in: path
 *     required: true
 *     descripcion: ID
 *     responses:
 *       200:
 *         Sucess
 */
server.delete("/medios_de_pago/eliminar/:id",is_admin,validarDatosMediosPago,validarExistenciaFormadePago,(req,res)=>{
    FormasdePago = FormasdePago.filter(item => item.id !== parseInt(req.params.id))
    res.status(202).json("ELIMINACION DE MEDIO DE PAGO EXITOSA")
})

/**
 * @swagger
 * /medios_de_pago/modificar/{id}:
 *  put:
 *    description: MODIFICAR MEDIO DE PAGO
 *    parameters:
 *    - name: id
 *      type: string
 *      in: path
 *      requered: true
 *      descripcion: ID
 *    - name: formadepago
 *      type: string
 *      in: formData
 *      required: true
 *      description: MEDIO DE PAGO
 *    responses:
 *      200:
 *        Sucess
 */

server.put("/medios_de_pago/modificar/:id",is_admin,validarDatosMediosPago,validarExistenciaFormadePago,(req,res)=>{
    const{formadepago}=req.body
    FormasdePago.map(item =>{
        if(item.id === parseInt(req.params.id)){
            item.nom = formadepago
        }
    })
    res.status(202).json("MEDIOS DE PAGO MODIFICADO EXITOSAMENTE")
})

server.listen(puerto,()=>{console.log(`escuchando en puerto:${env.development.PORT}`)});
